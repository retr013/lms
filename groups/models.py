import random

from django.db import models

# Create your models here.
from faker import Faker


class Group(models.Model):

    teacher = models.CharField(max_length=64, null=False)
    teacher2 = models.CharField(max_length=84, null=False)
    students_amount = models.IntegerField(null=False, default=42)

    def __str__(self):
        return f'{self.teacher},' \
               f' {self.teacher2},' \
               f' {self.students_amount},' \


    @classmethod
    def generate_groups(cls, count):

        faker = Faker()

        for _ in range(count):
            obj = cls(
                teacher=faker.name(),
                teacher2=faker.name(),
                students_amount=random.randint(10, 30)
            )

            obj.save()
