import re

from django.core.exceptions import ValidationError
from django.forms import ModelForm

from groups.models import Group


class GroupBaseForm(ModelForm):

    class Meta:
        model = Group
        fields = '__all__'

    def as_div(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(label)s %(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )


class GroupCreateForm(GroupBaseForm):
    pass
