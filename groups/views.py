from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render # noqa

# Create your views here.
from django.views.decorators.csrf import csrf_exempt

from groups.forms import GroupCreateForm
from groups.models import Group
from students.utils import format_list


def get_groups(request):
    groups = Group.objects.all().order_by('-id')

    params = [
        'teacher',
        'teacher2',
        'students_amount'
    ]

    for param_name in params:
        param_value = request.GET.get(param_name)
        if param_value:
            param_elems = param_value.split(',')
            if param_elems:
                or_filter = Q()
                for param_elem in param_elems:
                    or_filter |= Q(**{param_name: param_elem})
                groups = groups.filter(or_filter)
            else:
                groups = groups.filter(**{param_name: param_value})

    form = """
    <form >

      <label >First name:</label><br>
      <input type="text" name="teacher1"><br>

      <label >Last name:</label><br>
      <input type="text" name="teacher2"><br>

      <label >Students:</label><br>
      <input type="number" name="students_amount"><br><br>

      <input type="submit" value="Submit">

    </form> 
    """

    result = format_list(groups)

    return HttpResponse(form + result)


@csrf_exempt
def create_group(request):
    if request.method == 'POST':

        form = GroupCreateForm(request.POST)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect('/groups/')

    elif request.method == 'GET':

        form = GroupCreateForm()

    html_template = """
    <form method='post'>
      {}

      <input type="submit" value="Create">
    </form> 
    """

    result = html_template.format(form.as_div())

    return HttpResponse(result)
