from django.apps import AppConfig


class StudentsConfig(AppConfig):
    name = 'students'


class TeachersConfig(AppConfig):
    name = 'teachers'


class GroupsConfig(AppConfig):
    name = 'groups'
