import re

from django.core.exceptions import ValidationError
from django.forms import ModelForm

from students.models import Student


class StudentBaseForm(ModelForm):

    class Meta:
        model = Student
        # fields = ['first_name', 'last_name', 'age']
        fields = '__all__'

    def as_div(self):
        "Return this form rendered as HTML <p>s."
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(label)s %(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )

    def clean_phone_number(self):
        SHORT_LENGTH = 13

        phone_number = self.cleaned_data['phone_number']

        pattern = '(\(\d\d\d\)|\+\d\d\(\d\d\d\))\d\d\d\-\d\d\d\d'

        if not re.match(pattern, phone_number):
            raise ValidationError('Phone number is not correct')

        if len(phone_number) == SHORT_LENGTH:
            phone_number = '+38' + phone_number

        return phone_number

    def clean(self):
        result = super().clean()

        enroll_date = self.cleaned_data['enroll_date']
        graduate_date = self.cleaned_data['graduate_date']

        if enroll_date > graduate_date:
            raise ValidationError('Enroll date coudn\'t be after graduate date')

        return result

    def clean_email(self):

        BLACKLIST = ['asd.com']

        email = self.cleaned_data['email']

        students = Student.objects.filter(email=email).exclude(id=self.instance.id)

        if students:
            raise ValidationError('Email is not unique')

        return email


class StudentCreateForm(StudentBaseForm):
    pass


class StudentUpdateForm(StudentBaseForm):
    class Meta(StudentBaseForm.Meta):
        exclude = ['age']
