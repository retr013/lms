import datetime
import random

from django.db import models

# Create your models here.
from faker import Faker


class Teacher(models.Model):

    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)
    age = models.IntegerField(default=42, null=False)
    email = models.EmailField(max_length=64)
    phone_number = models.CharField(null=False, max_length=20)
    birth_date = models.DateField(null=False, default=datetime.datetime(1970, 1, 1))


    def __str__(self):
        return f'{self.first_name}, {self.last_name}, {self.age}'

    @classmethod
    def generate_teachers(cls, count):

        faker = Faker()

        for _ in range(count):
            obj = cls(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                age=random.randint(15, 105)
            )

            obj.save()
