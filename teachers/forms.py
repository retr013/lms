import re # noqa

from django.core.exceptions import ValidationError # noqa
from django.forms import ModelForm

from teachers.models import Teacher


class TeacherBaseForm(ModelForm):

    class Meta:
        model = Teacher
        fields = '__all__'

    def as_div(self):
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(label)s %(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )


class TeacherCreateForm(TeacherBaseForm):
    pass


class TeacherUpdateForm(TeacherBaseForm):
    class Meta(TeacherBaseForm.Meta):
        exclude = ['age']
