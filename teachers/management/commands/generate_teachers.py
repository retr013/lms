from django.core.management.base import BaseCommand
from teachers.models import Teacher


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('num', type=int)

    def handle(self, *args, **kwargs):
        num = kwargs['num']
        Teacher.generate_teachers(num)
        
